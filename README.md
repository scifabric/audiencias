# Frontend server for audiencias.pybossa.com

This is the frontend server built for aggregating data from social media and
marketing campaigns into a PYBOSSA server and query its values based on different
filters.

The server is built using Nuxt.js and PYBOSSA.


## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

## Financial support

 - Ayudas para la modernización e innovación de las industrias culturales y creativas mediante proyectos digitales y tecnológicos. Convocatoria 2017
 - Ayudas para la acción y la promoción cultural. Convocatoria de 2017
