const localConfig = require('./localConfig')
const path = require('path')

module.exports = {
  plugins: ['~/plugins/buefy.js'],
  /*
  ** Headers of the page
  */
  head: {
    title: 'frontend',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Audiencias' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  modules: [
    '@nuxtjs/bulma',
    '@nuxtjs/proxy',
    '@nuxtjs/axios',
    'nuxt-buefy',
    'vue-wait/nuxt'
  ],
  axios: {
    baseURL: localConfig.baseURL,
    withCredentials: true,
    credentials: true,
    proxyHeaders: true,
    requestInterceptor: (config) => {
      config.headers['Content-Type'] = 'application/json'
      if (config.data && config.data.hasOwnProperty('csrf')) {
        config.headers['X-CSRFToken'] = config.data.csrf
      }
      if (config.data === undefined) {
        config.data = {}
      }
      if (config.data && Object.prototype.toString.call(config.data) === '[object FormData]') {
        config.headers['X-CSRFToken'] = config.data.get('csrf')
      }
      return config
    }
  },
  proxy: {
    '/api': localConfig.baseURL
  }

}

